<?php

use Dotenv\Parser\Value;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'App\Http\Controllers\HomeController')->name('home');

Route::get('produtos', 'App\Http\Controllers\CategoryController@index')->name('products');
Route::get('produtos/{slug}', 'App\Http\Controllers\CategoryController@show')->name('products.category');

Route::get('blog', 'App\Http\Controllers\BlogController')->name('blog');

Route::view('sobre', 'site.about.index')->name('about');

Route::get('contato', 'App\Http\Controllers\ContactController@index')->name('contact');
Route::post('contato', 'App\Http\Controllers\ContactController@form')->name('contact.form');
